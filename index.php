<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "vendor/autoload.php";
require_once 'config.php';

$contentFields = array(
    'name'      => 'Name',
    'email'     => 'Email address',
    'subject'   => 'Subject',
    'message'   => 'Message'
);

// echo "<br>Attributes for POST: " . count($_POST);
// echo "<br>Attributes for GET: " . count($_GET);

// get attributes from GET or POST
if (count($_POST)>0) {
    $attribs = $_POST;
} else if (count($_GET)>0) {
    $attribs = $_GET;
} else {
    echo "<br>No attributes given!";
    http_response_code(400);
    exit();
}

// check if correct amount of attributes is given
if (count($attribs) != count($contentFields)) {
    echo "<br>Wrong amount of attributes given!";
    http_response_code(400);
    exit();
}

// build html mail
$htmlMail = '<b>The message below was sent via the NFDI4Earth support form</b><br><br>';
foreach ($attribs as $key => $value) {
    if (isset($contentFields[$key]) && strlen($value)>0) {

        if ($key == 'message') {
            $htmlMail .= "<br><b>$contentFields[$key]:</b> <br>";

            $htmlMail .= nl2br($value);
        }
        else {
            $htmlMail .= "<b>$contentFields[$key]:</b> $value<br>";
        }
    } else {
        echo "<br>Not all attributes have values!";
        http_response_code(400);
        exit();
    }
}

//Create a new PHPMailer instance
$mail = new PHPMailer();
//Tell PHPMailer to use SMTP
$mail->isSMTP();

// ***DEBUGGING STUFF***
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = SMTP_DEBUG;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

// ***FIXED CONFIGURATIONS***
$mail->Port = 587;
$mail->SMTPAuth = true;
$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

// ***CONFIGURATIONS from config.php***
$mail->Host = SMTP_SERVER;
$mail->Username = SMTP_USER;
$mail->Password = SMTP_PASSWORD;
$mail->addAddress(RECIPIENT_MAIL, RECIPIENT_NAME);

// ***CONFIGURATIONS from attributes***
$mail->Subject = $attribs['subject'];
$mail->addReplyTo($attribs['email'], $attribs['name']);
$mail->setFrom($attribs['email'], $attribs['name']);
$mail->msgHTML($htmlMail);

//send the message and check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
    http_response_code(400);
} else {
    echo "Message sent!";
    http_response_code(200);
} 
