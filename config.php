<?php
    
    define('SMTP_DEBUG', 1);
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages

    define('SMTP_SERVER', );
    // e.g.: define('SMTP_SERVER', 'msx.tu-dresden.de');
    define('SMTP_USER', );
    // e.g.: define('SMTP_USER', 'my_login');
    define('SMTP_PASSWORD', );
    // e.g.: define('SMTP_USER', 'my_password');

    // MAIL ADRESS, WHERE MAIL GOES TO
    // should be the helpdesk mailadress
    define('RECIPIENT_MAIL', );
    // e.g.: define('RECIPIENT_MAIL', 'helpdesk@nfdi4earth.de');
    define('RECIPIENT_NAME', );
    // e.g.: define('RECIPIENT_NAME', 'NFDI4Earth - Helpdesk');